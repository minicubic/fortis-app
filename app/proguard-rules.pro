# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

-keepattributes *Annotation*

##---------------Begin: proguard configuration for okHttp3 ---------
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn com.squareup.okhttp.internal.huc.**
##---------------End: proguard configuration for okHttp ------------


##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

-keepattributes EnclosingMethod

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.FieldNamingStrategy { *; }
-keep public class com.google.gson.**
-keep public class com.google.gson.** {public private protected *;}

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
-keep class com.fortis.scannerapp.models.** { *; }
##---------------End: proguard configuration for Gson  ----------


##---------------Begin: proguard configuration common for all Android apps ----------
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.MapActivity
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class org.apache.commons.io.**
##---------------End: proguard configuration common for all Android apps ----------


###---------------Begin: proguard configuration for BarcodeScaner Lib  ----------
#-keep class me.dm7.barcodescanner.** { *; }
-keep class net.sourgeforge.zbar.** { *; }
#-keepclassmembers enum * {
#    public static *[]; values(*;);
#    public static *; values(*;);
#    public static *; valueOf(java.lang.String);
#}
###---------------End: proguard configuration for BarcodeScaner Lib ----------


###---------------Begin: proguard configuration for SweetAlert  ----------
#-keep class cn.pedant.SweetAlert.** { *; }
###---------------End: proguard configuration for SweetAlert  ----------