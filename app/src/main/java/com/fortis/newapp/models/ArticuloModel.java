package com.fortis.newapp.models;

import java.io.Serializable;

/**
 * Created by Gustavo on 7/4/2016.
 */
public class ArticuloModel implements Serializable {
    private String CodigoInterno;
    private String Descripcion;
    private String CodigoBarra;
    private int Contenido;
    private float Precio;
    private float PrecioVtaCaja;
    private float PrecioUnitarioRS;
    private float PrecioUnitarioUS;
    private float PrecioMayoristaRS;
    private float PrecioMayoristaUS;
    private float TotalMayorista;
    private float TotalMayoristaRS;
    private float TotalMayoristaUS;
    private float CotizacionRS;
    private float CotizacionUS;

    public String getCodigoInterno() {
        return CodigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        CodigoInterno = codigoInterno;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCodigoBarra() {
        return CodigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        CodigoBarra = codigoBarra;
    }

    public int getContenido() {
        return Contenido;
    }

    public void setContenido(int contenido) {
        Contenido = contenido;
    }

    public float getPrecio() {
        return Precio;
    }

    public void setPrecio(float precio) {
        Precio = precio;
    }

    public float getPrecioVtaCaja() {
        return PrecioVtaCaja;
    }

    public void setPrecioVtaCaja(float precioVtaCaja) {
        PrecioVtaCaja = precioVtaCaja;
    }

    public float getPrecioUnitarioRS() {
        return PrecioUnitarioRS;
    }

    public void setPrecioUnitarioRS(float precioUnitarioRS) {
        PrecioUnitarioRS = precioUnitarioRS;
    }

    public float getPrecioUnitarioUS() {
        return PrecioUnitarioUS;
    }

    public void setPrecioUnitarioUS(float precioUnitarioUS) {
        PrecioUnitarioUS = precioUnitarioUS;
    }

    public float getPrecioMayoristaRS() {
        return PrecioMayoristaRS;
    }

    public void setPrecioMayoristaRS(float precioMayoristaRS) {
        PrecioMayoristaRS = precioMayoristaRS;
    }

    public float getPrecioMayoristaUS() {
        return PrecioMayoristaUS;
    }

    public void setPrecioMayoristaUS(float precioMayoristaUS) {
        PrecioMayoristaUS = precioMayoristaUS;
    }

    public float getTotalMayorista() {
        return TotalMayorista;
    }

    public void setTotalMayorista(float totalMayorista) {
        TotalMayorista = totalMayorista;
    }

    public float getTotalMayoristaRS() {
        return TotalMayoristaRS;
    }

    public void setTotalMayoristaRS(float totalMayoristaRS) {
        TotalMayoristaRS = totalMayoristaRS;
    }

    public float getTotalMayoristaUS() {
        return TotalMayoristaUS;
    }

    public void setTotalMayoristaUS(float totalMayoristaUS) {
        TotalMayoristaUS = totalMayoristaUS;
    }

    public float getCotizacionRS() {
        return CotizacionRS;
    }

    public void setCotizacionRS(float cotizacionRS) {
        CotizacionRS = cotizacionRS;
    }

    public float getCotizacionUS() {
        return CotizacionUS;
    }

    public void setCotizacionUS(float cotizacionUS) {
        CotizacionUS = cotizacionUS;
    }
}
