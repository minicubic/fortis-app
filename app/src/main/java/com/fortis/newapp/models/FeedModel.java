package com.fortis.newapp.models;

import java.io.Serializable;

/**
 * Created by Gustavo on 7/4/2016.
 */
public class FeedModel implements Serializable {
    private String FotoUrl;
    private String FotoHDUrl;
    private String Descripcion;

    public String getFotoUrl() {
        return FotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        FotoUrl = fotoUrl;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFotoHDUrl() {
        return FotoHDUrl;
    }

    public void setFotoHDUrl(String fotoHDUrl) {
        FotoHDUrl = fotoHDUrl;
    }
}
