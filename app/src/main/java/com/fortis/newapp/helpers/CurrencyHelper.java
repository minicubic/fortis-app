package com.fortis.newapp.helpers;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Gustavo on 13/10/2015.
 */
public class CurrencyHelper {

    public static String getCurrencyFormattedString(double value, Moneda moneda) {
        Locale locale = null;
        switch (moneda) {
            case Guaranies:
                locale = new Locale("es", "PY");
                break;
            case Reales:
                locale = new Locale("pt", "BR");
                break;
            case Dolares:
                locale = new Locale("en", "US");
                break;
        }
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        DecimalFormatSymbols dfs = new DecimalFormatSymbols(locale);
        ((DecimalFormat) numberFormat).setDecimalFormatSymbols(dfs);
        numberFormat.setMaximumFractionDigits(2);

        try {
            return numberFormat.format(value);
        } catch (Exception e) {
            return "";
        }
    }

    public enum Moneda {
        Guaranies,
        Reales,
        Dolares
    }

}
