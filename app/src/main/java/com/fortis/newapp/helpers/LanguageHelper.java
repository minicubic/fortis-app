package com.fortis.newapp.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.fortis.newapp.app.Config;

import java.util.Locale;

/**
 * Created by Gustavo on 26/04/2016.
 */
public class LanguageHelper {

    public static void setLanguage(Context context, String idioma) {
        String locale = "";
        String savedLanguage = "";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (TextUtils.isEmpty(idioma)) {
            savedLanguage = prefs.getString(Config.PREFERENCIAS.PROPERTY_LENGUAJE, Config.IDIOMAS.Español);

        } else {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(Config.PREFERENCIAS.PROPERTY_LENGUAJE, idioma);
            editor.apply();

            savedLanguage = prefs.getString(Config.PREFERENCIAS.PROPERTY_LENGUAJE, Config.IDIOMAS.Español);
        }

        if (savedLanguage.equals(Config.IDIOMAS.Español)) {
            locale = Config.LOCALES.Español;
        }
        if (savedLanguage.equals(Config.IDIOMAS.Portugues)) {
            locale = Config.LOCALES.Portugues;
        }

        String[] mLang = locale.split("_");
        Locale myLocale = new Locale(mLang[0].trim(), mLang[1].trim());

        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

}
