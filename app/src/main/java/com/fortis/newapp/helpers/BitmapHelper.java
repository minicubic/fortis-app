package com.fortis.newapp.helpers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * Created by Gustavo on 10/05/2016.
 */
public class BitmapHelper {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static String GuardarFotoLocalmente(Bitmap bitmap, Context ctx) {
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory()
                        + "/Android/data/" + ctx.getPackageName() + "/Files");

        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String filePath;
        Date now = new Date();
        String fileName = now.getTime() + ".jpg";

        filePath = (mediaStorageDir.getAbsolutePath() + "/" + fileName);
        FileOutputStream fileOutputStream;

        try {

            fileOutputStream = new FileOutputStream(filePath);
            fileOutputStream.write(bytes.toByteArray());
            // remember close de FileOutput
            fileOutputStream.close();

            return filePath;
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return "";
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static boolean SaveImageToExternalStorage(Bitmap image, Context context, boolean bool) {
        String dirPath = "/Fortis/";
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + dirPath;

        try {

            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            Date now = new Date();
            String fileName = now.getTime() + ".jpg";

            OutputStream outputStream;
            File file = new File(fullPath, fileName);
            file.createNewFile();
            outputStream = new FileOutputStream(file);

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            // refresh gallery after save image
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

                Uri contentUri = Uri.fromFile(file); //the file you saved/deleted/moved/copied
                mediaScanIntent.setData(contentUri);
                context.sendBroadcast(mediaScanIntent);

            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }

            return true;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static String SaveImageToExternalStorage(Bitmap image, Context context) {
        String dirPath = "/Fortis/";
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + dirPath;

        try {

            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            Date now = new Date();
            String fileName = now.getTime() + ".jpg";

            OutputStream outputStream;
            File file = new File(fullPath, fileName);
            file.createNewFile();
            outputStream = new FileOutputStream(file);

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            // refresh gallery after save image
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

                Uri contentUri = Uri.fromFile(file); //the file you saved/deleted/moved/copied
                mediaScanIntent.setData(contentUri);
                context.sendBroadcast(mediaScanIntent);

            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }

            return fullPath + "/" + fileName;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return "";
        }
    }

}
