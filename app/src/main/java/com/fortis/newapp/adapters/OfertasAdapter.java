package com.fortis.newapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.fortis.newapp.R;
import com.fortis.newapp.models.FeedModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Gustavo on 21/3/2016.
 */
public class OfertasAdapter extends
        RecyclerView.Adapter<OfertasAdapter.MyViewHolder> {

    Context context;
    ArrayList<FeedModel> modelArrayList;
    private AdapterItemClickCallback adapterItemClickCallback;

    public OfertasAdapter() {
        this.modelArrayList = new ArrayList<>();
    }

    public void add(FeedModel item) {
        if (this.modelArrayList == null) {
            this.modelArrayList = new ArrayList<>();
        }
        this.modelArrayList.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        if (this.modelArrayList != null) {
            this.modelArrayList.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_view_ofertas, parent, false);

        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        FeedModel item = this.modelArrayList.get(position);

        Picasso.with(context)
                .load(item.getFotoUrl())
                .fit()
                .centerCrop()
                .into(holder.imgFoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    @Override
    public int getItemCount() {
        return this.modelArrayList.size();
    }

    public void setAdapterItemClickCallback(AdapterItemClickCallback clickCallBack) {
        this.adapterItemClickCallback = clickCallBack;
    }

    public interface AdapterItemClickCallback {
        void onAdapterClicked(FeedModel item);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgFoto;
        ProgressBar progressBar;
//        TextView txtDescripcion;

        MyViewHolder(View itemView) {
            super(itemView);

//            txtDescripcion = (TextView) itemView.findViewById(R.id.txtContenido);
            imgFoto = (ImageView) itemView.findViewById(R.id.imageView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (adapterItemClickCallback != null) {
                        if (getAdapterPosition() >= 0) {
                            adapterItemClickCallback.onAdapterClicked(modelArrayList.get(getAdapterPosition()));
                        } else {
                            adapterItemClickCallback.onAdapterClicked(modelArrayList.get(0));
                        }
                    }
                }
            });
        }
    }

}
