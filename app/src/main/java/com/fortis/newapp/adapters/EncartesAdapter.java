package com.fortis.newapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.fortis.newapp.R;
import com.fortis.newapp.models.FeedModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Gustavo on 21/3/2016.
 */
public class EncartesAdapter extends
        RecyclerView.Adapter<EncartesAdapter.MyViewHolder> {

    Context context;
    int mWidth, mHeight;
    ArrayList<FeedModel> modelArrayList;
    private AdapterItemClickCallback adapterItemClickCallback;

    public EncartesAdapter() {
        this.modelArrayList = new ArrayList<>();
    }

    public void setWidthHeight(int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
    }

    public void add(FeedModel item) {
        if (this.modelArrayList == null) {
            this.modelArrayList = new ArrayList<>();
        }
        this.modelArrayList.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        if (this.modelArrayList != null) {
            this.modelArrayList.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_view_folleto_snapp, parent, false);

        item.getLayoutParams().width = mWidth;
        item.getLayoutParams().height = mHeight;

        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        FeedModel item = this.modelArrayList.get(position);

        Picasso.with(context)
                .load(item.getFotoUrl())
                .fit()
                .centerCrop()
                .into(holder.imgFoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.btnDownload.setEnabled(true);
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    @Override
    public int getItemCount() {
        return this.modelArrayList.size();
    }

    public void setAdapterItemClickCallback(AdapterItemClickCallback clickCallBack) {
        this.adapterItemClickCallback = clickCallBack;
    }

    public interface AdapterItemClickCallback {
        void onDownloadButtonClicked(FeedModel item);

        void onImageClicked(FeedModel item);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgFoto;
        ProgressBar progressBar;
        ImageButton btnDownload;

        MyViewHolder(View itemView) {
            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

            btnDownload = (ImageButton) itemView.findViewById(R.id.btnDownload);
            btnDownload.setEnabled(false);
            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (adapterItemClickCallback != null) {
                        adapterItemClickCallback.onDownloadButtonClicked(modelArrayList.get(getAdapterPosition()));
                    }
                }
            });

            imgFoto = (ImageView) itemView.findViewById(R.id.imageView);
            imgFoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterItemClickCallback != null) {

                        if (getAdapterPosition() >= 0) {
                            adapterItemClickCallback.onImageClicked(modelArrayList.get(getAdapterPosition()));
                        } else {
                            adapterItemClickCallback.onImageClicked(modelArrayList.get(0));
                        }

                    }
                }
            });
        }
    }

}
