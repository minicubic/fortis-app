package com.fortis.newapp.gcmhelper;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.fortis.newapp.R;
import com.fortis.newapp.activities.EncartesActivity;
import com.fortis.newapp.activities.OfertasActivity;
import com.fortis.newapp.app.Config;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Random;

public class GCMIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;

    public GCMIntentService() {
        super("GcmIntentService");
    }

    public static boolean readPref(Context context, String key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getBoolean(key, true);
    }

    @SuppressWarnings({"deprecation", "StatementWithEmptyBody"})
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                String mensaje = extras.getString("message");
                sendNotification(mensaje);

            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        // NotificationType|Title
        String[] msgValues = msg.split("\\|");
        String notificationType = msgValues[0];
        String notificationTitle = msgValues[1];

        Intent openFromNotificationIntent = null;
        if (notificationType.equalsIgnoreCase("Oferta")) {
            openFromNotificationIntent = new Intent(getApplicationContext(), OfertasActivity.class);
        }
        if (notificationType.equalsIgnoreCase("Encarte")) {
            openFromNotificationIntent = new Intent(getApplicationContext(), EncartesActivity.class);
        }

        PendingIntent pendingIntent;
        pendingIntent = PendingIntent.getActivity(
                getApplicationContext(), new Random().nextInt(),
                openFromNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        boolean receiveNotifications, vibrateWithNotifications;

        receiveNotifications = readPref(this, Config.PREFERENCIAS.PROPERTY_RECIBIR_NOTIFICACIONES);
        vibrateWithNotifications = readPref(this, Config.PREFERENCIAS.PROPERTY_VIBRAR);

        NotificationCompat.Builder mBuilder;
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (receiveNotifications) {
            mBuilder = getNotificationBuilder(notificationTitle, notificationSound, vibrateWithNotifications);
            mBuilder.setContentIntent(pendingIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }

    private NotificationCompat.Builder getNotificationBuilder(String tituloNotificacion,
                                                              Uri notificationSound,
                                                              boolean vibrate) {
        if (vibrate) {
            return new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(tituloNotificacion))
                    .setContentText(tituloNotificacion)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000})
                    .setLights(Color.BLUE, 3000, 3000)
                    .setSound(notificationSound)
                    .setTicker(tituloNotificacion);
        } else {
            return new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(tituloNotificacion))
                    .setContentText(tituloNotificacion)
                    .setAutoCancel(true)
                    .setLights(Color.BLUE, 3000, 3000)
                    .setSound(notificationSound)
                    .setTicker(tituloNotificacion);
        }
    }
}
