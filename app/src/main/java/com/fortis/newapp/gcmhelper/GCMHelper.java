package com.fortis.newapp.gcmhelper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.fortis.newapp.app.Config;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by Gustavo on 05/02/2015.
 */
public class GCMHelper {

    Context context;
    GoogleCloudMessaging googleCloudMessaging;

    public GCMHelper(Context _context) {
        context = _context;
    }

    public boolean registerGCMClient() {
        GetGoogleRegistrationID getGoogleRegistrationID = new GetGoogleRegistrationID();
        getGoogleRegistrationID.execute();
        return true;
    }

    public void storeRegistrationId(Context context, String regId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Config.PREFERENCIAS.PROPERTY_REG_ID, regId);
        editor.apply();
    }

    public String getRegistrationId(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(Config.PREFERENCIAS.PROPERTY_REG_ID, "");
    }

    private class GetGoogleRegistrationID extends AsyncTask<Void, Void, String> {

        String registrationID;
        String googlePlayAssociatedEmail;

        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(Void... params) {
            try {
                if (googleCloudMessaging == null) {
                    googleCloudMessaging = GoogleCloudMessaging.getInstance(context);
                }

                registrationID = googleCloudMessaging.register(Config.GCM_CONFIG.SENDER_ID);

                Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");
                if (accounts != null) {
                    if (accounts.length > 0) {
//                        gets the gmail associated to this device
                        googlePlayAssociatedEmail = accounts[0].name;
                    } else {
                        googlePlayAssociatedEmail = "no_mail";
                    }
                } else {
                    googlePlayAssociatedEmail = "no_mail";
                }

            } catch (IOException ex) {
                registrationID = "ERROR";
                // ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return registrationID;
        }

        @Override
        protected void onPostExecute(String resultID) {
            // Persist the regID - no need to register again.
            if (!resultID.isEmpty() && !resultID.equals(("ERROR"))) {
                // send to server
                RegisterToServer registerToServer =
                        new RegisterToServer(
                                context,
                                registrationID,
                                googlePlayAssociatedEmail);
                registerToServer.execute();

            }
        }
    }

    private class RegisterToServer extends AsyncTask<Void, Void, Boolean> {

        Context context;
        String userEmail = "";
        String registrationID = "";

        public RegisterToServer(Context ctx,
                                String mRegistrationID,
                                String mUserEmail) {
            context = ctx;
            userEmail = mUserEmail;
            registrationID = mRegistrationID;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                storeRegistrationId(context, registrationID);
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // You should send the registration ID to your server over HTTP,
            // so it can use GCM/HTTP or CCS to send messages to your app.

            OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody requestBody = new FormEncodingBuilder()
                    .add(Config.KEY.DeviceRegistrationId, registrationID)
                    .add(Config.KEY.Email, userEmail)
                    .add(Config.KEY.AuthToken, Config.WEB_API.authToken)
                    .build();

            Request postRequest = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .post(requestBody)
                    .url(Config.WEB_API.registerGCMuser)
                    .build();

            Response response = null;
            try {
                response = okHttpClient.newCall(postRequest).execute();
                return response.isSuccessful();

            } catch (IOException ignored) {
            }
            return false;
        }
    }

}
