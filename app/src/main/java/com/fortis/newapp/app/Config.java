package com.fortis.newapp.app;

import com.fortis.newapp.BuildConfig;

/**
 * Created by Gustavo on 21/3/2016.
 * @author xergio
 * @version 2 - 04/04/2017
 */
public class Config {

    private static String API_SERVER_ROOT = BuildConfig.API_SERVER_ROOT;
    public  static String APP_MAYORISTA_EXTERNA_PACKAGE = "py.com.upload.fortisapp";
    public  static String APP_MAYORISTA_EXTERNA_PLAYSTORE_LINK = "https://play.google.com/store/apps/details?id=py.com.upload.fortisapp";
    public  static String APP_MAYORISTA_API = BuildConfig.APP_MAYORISTA_API;

    public static class WEB_API {
        public static String authToken = "dec261E94ad1_8a576f0d";
        public static String recuperarOfertas = API_SERVER_ROOT + "ofertas/get/" + authToken;
        public static String recuperarFolletos = API_SERVER_ROOT + "folletos/get/" + authToken;
        public static String consultarArticulo = API_SERVER_ROOT + "articulos/consultar/@codigobarra/" + authToken;
        public static String registerGCMuser = API_SERVER_ROOT + "gcm/users/register";
    }

    public static class GCM_CONFIG {
        public static String SENDER_ID = BuildConfig.GCM_SENDER_ID;
    }

    public static class KEY {
        public static String Email = "Email";
        public static String AuthToken = "AuthToken";
        public static String DeviceRegistrationId = "DeviceRegistrationId";
    }

    public static class PREFERENCIAS {
        public static String PROPERTY_LENGUAJE = "Lenguaje";
        public static String PROPERTY_REG_ID = "RegistrationID";
        public static String PROPERTY_RECIBIR_NOTIFICACIONES = "notifications_receive";
        public static String PROPERTY_VIBRAR = "notifications_vibrate";
        public static String PROPERTY_FORTIS_ASOCIADO_INSTALLED = "fortis_asociado_installed";
    }

    public static class LOCALES {
        public static String Español = "es_PY";
        public static String Portugues = "pt_BR";
    }

    public static class IDIOMAS {
        public static String Español = "Español";
        public static String Portugues = "Portugues";
    }


}
