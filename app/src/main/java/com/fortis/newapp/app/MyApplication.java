package com.fortis.newapp.app;

import android.Manifest;
import android.app.Application;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.fortis.newapp.R;
import com.holidaycheck.permissify.DialogText;
import com.holidaycheck.permissify.PermissifyConfig;

import java.util.HashMap;

/**
 * Created by Gustavo on 12/10/2016.
 */

public class MyApplication extends Application {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate() {
        super.onCreate();

        // permissions config
        PermissifyConfig permissifyConfig = new PermissifyConfig.Builder()
                .withDefaultTextForPermissions(new HashMap<String, DialogText>() {{
                    put(Manifest.permission_group.CAMERA, new DialogText(
                            R.string.permission_rationale_camera,
                            R.string.permission_denial_camera));
                }})
                .build();

        PermissifyConfig.initDefault(permissifyConfig);
    }

}
