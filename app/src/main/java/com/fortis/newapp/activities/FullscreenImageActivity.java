package com.fortis.newapp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.fortis.newapp.R;
import com.fortis.newapp.helpers.BitmapHelper;
import com.fortis.newapp.helpers.LanguageHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import uk.co.senab.photoview.PhotoViewAttacher;

public class FullscreenImageActivity extends AppCompatActivity {

    ImageView imageView;
    ProgressBar progressBar;
    PhotoViewAttacher mAttacher;
    private String FOTO_URL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(this, "");
        setContentView(R.layout.activity_fullscreen_image);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imageView = (ImageView) findViewById(R.id.imageView);
        FOTO_URL = getIntent().getStringExtra("URL");

        Picasso.with(this)
                .load(FOTO_URL)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (bitmap != null) {

                            imageView.setImageBitmap(bitmap);

                            // hide progressbar
                            progressBar.setVisibility(View.GONE);

                            // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
                            // (not needed unless you are going to change the drawable later)
                            mAttacher = new PhotoViewAttacher(imageView);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.full_screen_image_title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.item_menu_share_image) {
            shareImage();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareImage() {

//        setType("image/png"); OR for jpeg: setType("image/jpeg");
//        shareIntent.setType("image/*");
        progressBar.setVisibility(View.VISIBLE);

        Picasso.with(this)
                .load(FOTO_URL)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                        if (bitmap != null) {

                            String imagePath =
                                    BitmapHelper.SaveImageToExternalStorage(bitmap, FullscreenImageActivity.this);

                            if (!TextUtils.isEmpty(imagePath)) {
                                File imageFileToShare = new File(imagePath);
                                Uri uri = Uri.fromFile(imageFileToShare);

                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("image/jpeg");
                                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

                                startActivity(Intent.createChooser(shareIntent,
                                        getString(R.string.compartir_foto)));

                                progressBar.setVisibility(View.GONE);
                            }
                        }

                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }

}
