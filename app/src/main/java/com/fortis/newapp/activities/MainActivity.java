package com.fortis.newapp.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.fortis.newapp.R;
import com.fortis.newapp.app.Config;
import com.fortis.newapp.gcmhelper.GCMHelper;
import com.fortis.newapp.helpers.LanguageHelper;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.holidaycheck.permissify.PermissifyActivity;
import com.holidaycheck.permissify.PermissifyManager;

import java.util.List;

public class MainActivity extends PermissifyActivity {

    private static int REQUEST_INVITE = 0;
    private static final int CAMERA_PERMISSION_REQUEST_ID = 2;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    View rootLayout;
    ImageButton btnFacebook;
    RippleView rippleViewOfertas;
    RippleView rippleViewFolletos;
    RippleView rippleViewAppMayorista;
    RippleView rippleViewConsultarPrecios;
    RippleView rippleViewRevista;
    private GoogleApiClient googleApiClient;
    TextView lblFortisAsociadoDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(this, "");
        setContentView(R.layout.activity_main);

        rootLayout = findViewById(R.id.rootLayout);

        rippleViewConsultarPrecios = (RippleView) findViewById(R.id.rippleViewConsultarPrecios);
        rippleViewConsultarPrecios.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {

                // check camera permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getPermissifyManager().callWithPermission(
                            MainActivity.this,
                            CAMERA_PERMISSION_REQUEST_ID,
                            Manifest.permission.CAMERA);
                } else {
                    openScannerActivity();
                }

            }
        });

        rippleViewOfertas = (RippleView) findViewById(R.id.rippleViewOfertas);
        rippleViewOfertas.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(MainActivity.this, OfertasActivity.class);
                startActivity(intent);
            }
        });

        rippleViewFolletos = (RippleView) findViewById(R.id.rippleViewFolletos);
        rippleViewFolletos.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(MainActivity.this, EncartesActivity.class);
                startActivity(intent);
            }
        });

        rippleViewAppMayorista = (RippleView) findViewById(R.id.rippleViewAppMayorista);
        rippleViewAppMayorista.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                //abrirAppMayoristaExterna();
                Intent intent = new Intent(MainActivity.this, FortisMayoristaActivity.class);
                startActivity(intent);
            }
        });

        rippleViewRevista = (RippleView) findViewById(R.id.rippleViewRevista);
        rippleViewRevista.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(MainActivity.this, RevistaActivity.class);
                startActivity(intent);
            }
        });

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(AppInvite.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.d("AppInvite", "onConnected");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.d("AppInvite", "onConnectionSuspended");
                    }
                })
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.d("AppInvite", "error");
                    }
                })
                .build();

        btnFacebook = (ImageButton) findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFacebookPage();
            }
        });

        // verificar que el dispositivo tenga instalado Google Play Services
        if (checkPlayServices()) {
            GCMHelper gcmHelper = new GCMHelper(this);
            // si no existe un id de registro almacenado, posiblemente el dispositivo
            // aún no ha sido registrado para recibir notificaciones de GCM
            String regID = gcmHelper.getRegistrationId(this);
            if (regID.equals((""))) {
                // registrar cliente GCM
                gcmHelper.registerGCMClient();
            }
        }

        getWifiIpAddress();
    }

    private boolean isFortisAsociadoInstalled() {
        String packageName = Config.APP_MAYORISTA_EXTERNA_PACKAGE;
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        return intent != null;
    }

    private void abrirAppMayoristaExterna() {
        // si la app no está instalada, abrir Playstore para instalarla
        if (!isFortisAsociadoInstalled()) {

            Intent externalAppIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + Config.APP_MAYORISTA_EXTERNA_PACKAGE));
            boolean marketFound = false;

            // find all applications able to handle our rateIntent
            final List<ResolveInfo> otherApps = getPackageManager().queryIntentActivities(externalAppIntent, 0);
            for (ResolveInfo otherApp : otherApps) {
                // look for Google Play application
                if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

                    ActivityInfo otherAppActivity = otherApp.activityInfo;
                    ComponentName componentName = new ComponentName(
                            otherAppActivity.applicationInfo.packageName,
                            otherAppActivity.name
                    );
                    externalAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    externalAppIntent.setComponent(componentName);
                    startActivity(externalAppIntent);
                    marketFound = true;
                    break;
                }
            }
            // if GP not present on device, open web browser
            if (!marketFound) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Config.APP_MAYORISTA_EXTERNA_PLAYSTORE_LINK));
                startActivity(webIntent);
            }

        } else {
            // app instalada, abrir
            String packageName = Config.APP_MAYORISTA_EXTERNA_PACKAGE;
            Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
            startActivity(intent);
        }

    }

    private void openScannerActivity() {
        if (getWiFiInfo() != null) {

            if (!TextUtils.isEmpty(getWiFiInfo().getSSID())) {

                // validar contra SSID de la red WIFI nueva
                if (getWiFiInfo().getSSID().equalsIgnoreCase("\"FORTIS\"")) {

                    Intent intent = new Intent(MainActivity.this, ScannerActivity.class);
                    startActivity(intent);

                } else {
                    Snackbar.make(rootLayout,
                            R.string.debe_conectarse_a_wifi_fortis,
                            Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(rootLayout,
                        R.string.debe_conectarse_a_wifi_fortis,
                        Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(rootLayout,
                    R.string.debe_conectarse_a_wifi_fortis,
                    Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    @SuppressWarnings("deprecation")
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                return false;
            }
            return false;
        }
        return true;
    }

    private void openFacebookPage() {
        try {
            String facebookUrl = getFacebookPageURL();

            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
            facebookIntent.setData(Uri.parse(facebookUrl));

            startActivity(facebookIntent);
        } catch (Exception e) {
            Log.d("Fortis", e.getMessage());
        }
    }

    //method to get the right URL to use in the intent
    public String getFacebookPageURL() {
        String FACEBOOK_URL = "https://www.facebook.com/fortismayorista/";
        String FACEBOOK_PAGE_ID = "fortismayorista";

        PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_menu_recomendar) {
            invitarAmigos();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void invitarAmigos() {
        try {
            Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.app_invite_title))
                    .setMessage(getString(R.string.app_invite_msg))
                    .setDeepLink(Uri.parse("com.fortis.scannerapp.activities.StartActivity"))
                    //                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                    .setCallToActionText(getString(R.string.app_invite_call_to_action))
                    .build();
            startActivityForResult(intent, REQUEST_INVITE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Check how many invitations were sent and log a message
                // The ids array contains the unique invitation ids for each invitation sent
                // (one for each contact select by the user). You can use these for analytics
                // as the ID will be consistent on the sending and receiving devices.
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
            } else {
                // Sending failed or it was canceled, show failure message to the user
                Log.d("AppInvite", "error");
//                showMessage(getString(R.string.send_failed));
            }
        }
    }

    private String getWifiIpAddress() {
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        return Formatter.formatIpAddress(ip);
    }

    private WifiInfo getWiFiInfo() {
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return wifiInfo;
    }

    @Override
    public void onCallWithPermissionResult(int callId, PermissifyManager.CallRequestStatus status) {
        super.onCallWithPermissionResult(callId, status);
        if (callId == CAMERA_PERMISSION_REQUEST_ID) {
            switch (status) {
                case PERMISSION_GRANTED:
                    openScannerActivity();
                    break;

                case PERMISSION_DENIED_ONCE:
                    showCameraDenialOnceSnackbar();
                    break;

                case PERMISSION_DENIED_FOREVER:
                    break;

                case SHOW_PERMISSION_RATIONALE:
            }
        }
    }

    private void showCameraDenialOnceSnackbar() {
        Snackbar
                .make(findViewById(android.R.id.content), R.string.permission_denial_camera, Snackbar.LENGTH_LONG)
                .setAction(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        getPermissifyManager().onRationaleConfirmed(CAMERA_PERMISSION_REQUEST_ID);
                    }
                })
                .setActionTextColor(ContextCompat.getColor(this, android.R.color.holo_blue_bright))
                .show();
    }


    //    public Intent getFBIntent(Context context, String facebookId) {
//
//        try {
//            // Check if FB app is even installed
//            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
//
//            String facebookScheme = "fb://profile/" + facebookId;
//            return new Intent(Intent.ACTION_VIEW, Uri.parse(facebookScheme));
//        }
//        catch(Exception e) {
//
//            // Cache and Open a url in browser
//            String facebookProfileUri = "https://www.facebook.com/" + facebookId;
//            return new Intent(Intent.ACTION_VIEW, Uri.parse(facebookProfileUri));
//        }
//
//        return null;
//    }
//    In order to open the facebook app with a user profile all you need to do is:
//    Intent facebookIntent = getFBIntent(this, "2347633432");
//    startActivity(facebookIntent);

}
