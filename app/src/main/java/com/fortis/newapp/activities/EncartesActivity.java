package com.fortis.newapp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fortis.newapp.R;
import com.fortis.newapp.adapters.EncartesAdapter;
import com.fortis.newapp.app.Config;
import com.fortis.newapp.customviews.CenterLockListener;
import com.fortis.newapp.customviews.SnappingRecyclerView;
import com.fortis.newapp.helpers.BitmapHelper;
import com.fortis.newapp.helpers.Connectivity;
import com.fortis.newapp.helpers.LanguageHelper;
import com.fortis.newapp.models.FeedModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class EncartesActivity extends AppCompatActivity {

    View rootLayout;
    TextView txtEmpty;
    EncartesAdapter adapter;
    SnappingRecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    RecuperarFolletosAsync recuperarFolletosAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(this, "");
        setContentView(R.layout.activity_encartes);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        rootLayout = findViewById(R.id.rootLayout);
        txtEmpty = (TextView) findViewById(R.id.txtEmpty);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ejecutarRequest();
            }
        });

        adapter = new EncartesAdapter();
        adapter.setAdapterItemClickCallback(new EncartesAdapter.AdapterItemClickCallback() {

            @Override
            public void onDownloadButtonClicked(final FeedModel item) {

                new MaterialDialog.Builder(EncartesActivity.this)
                        .title(R.string.app_name)
                        .content(R.string.descargar_folleto_hd)
                        .positiveText(R.string.descargar)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                Toast.makeText(EncartesActivity.this,
                                        R.string.descargando_msg, Toast.LENGTH_SHORT).show();

                                // descargar imagen en HD
                                descargarImagenHD(item);
                                dialog.dismiss();
                            }
                        })
                        .negativeText(R.string.cancelar)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

            }

            @Override
            public void onImageClicked(FeedModel item) {
                if (item != null) {
                    Intent intent = new Intent(EncartesActivity.this, FullscreenImageActivity.class);
                    intent.putExtra("URL", item.getFotoHDUrl());
                    startActivity(intent);
                }
            }
        });

        recyclerView = (SnappingRecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int center = (recyclerView.getTop() + recyclerView.getBottom()) / 2;

                adapter.setWidthHeight(recyclerView.getWidth(), recyclerView.getHeight());
                recyclerView.setAdapter(adapter);
                recyclerView.addOnScrollListener(new CenterLockListener(center));

                try {
                    // Ensure you call it only once :
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                } catch (NoSuchMethodError ignored) {
                }
            }
        });

        ejecutarRequest();
    }

    private void ejecutarRequest() {
        Connectivity connectionState = new Connectivity(this);
        if (!connectionState.isConnectedToInternet()) {
            Toast.makeText(this,
                    R.string.error_msg_no_estas_conectado_a_internet,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        recuperarFolletosAsync = new RecuperarFolletosAsync();
        recuperarFolletosAsync.mActivity = this;
        recuperarFolletosAsync.execute();
    }

    private void manejarRespuesta(ArrayList<FeedModel> result) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        if (result != null) {
            adapter.clear();
            txtEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            for (int i = 0; i < result.size(); i++) {
                FeedModel item = result.get(i);
                adapter.add(item);
            }
        } else {
            txtEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void descargarImagenHD(FeedModel model) {
        Picasso.with(this)
                .load(model.getFotoHDUrl())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (bitmap != null) {

                            if (BitmapHelper.SaveImageToExternalStorage(bitmap,
                                    EncartesActivity.this, true)) {
                                Snackbar.make(rootLayout,
                                        R.string.imagen_descargada,
                                        Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
//    public boolean saveImageToExternalStorage(Bitmap image, String fileName) {
//        String dirPath = "/Fortis/";
//        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + dirPath;
//
//        try {
//
//            File dir = new File(fullPath);
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//
//            OutputStream outputStream;
//            File file = new File(fullPath, fileName + ".jpg");
//            file.createNewFile();
//            outputStream = new FileOutputStream(file);
//
//             100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
//            outputStream.flush();
//            outputStream.close();
//
//             refresh gallery after save image
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//
//                Uri contentUri = Uri.fromFile(file); //the file you saved/deleted/moved/copied
//                mediaScanIntent.setData(contentUri);
//                sendBroadcast(mediaScanIntent);
//
//            } else {
//                sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
//                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
//            }
//
//            return true;
//
//        } catch (Exception e) {
//            Log.e("saveToExternalStorage()", e.getMessage());
//            return false;
//        }
//    }

    private static class RecuperarFolletosAsync
            extends AsyncTask<Void, Void, ArrayList<FeedModel>> {

        Gson gson = new Gson();
        EncartesActivity mActivity;
        TypeToken<ArrayList<FeedModel>> typeToken = new TypeToken<ArrayList<FeedModel>>() {
        };

        @SuppressWarnings("TryWithIdenticalCatches")
        @Override
        protected ArrayList<FeedModel> doInBackground(Void... voids) {
            try {

                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(Config.WEB_API.recuperarFolletos)
                        .build();

                ArrayList<FeedModel> respuesta = null;
                Response response = okHttpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    Reader reader = response.body().charStream();
                    respuesta = gson.fromJson(reader, typeToken.getType());
                    reader.close();
                }

                return respuesta;
            } catch (IOException ignored) {

            } catch (JsonSyntaxException ignored) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedModel> feedModels) {
            super.onPostExecute(feedModels);
            if (mActivity != null) {
                mActivity.manejarRespuesta(feedModels);
                mActivity.recuperarFolletosAsync = null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mActivity != null) {
                mActivity.swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.swipeRefreshLayout.setRefreshing(true);
                    }
                });
            }
        }
    }
}
