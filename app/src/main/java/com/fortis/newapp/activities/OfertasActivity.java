package com.fortis.newapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.fortis.newapp.R;
import com.fortis.newapp.adapters.OfertasAdapter;
import com.fortis.newapp.app.Config;
import com.fortis.newapp.helpers.Connectivity;
import com.fortis.newapp.helpers.LanguageHelper;
import com.fortis.newapp.models.FeedModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class OfertasActivity extends AppCompatActivity {

    TextView txtEmpty;
    OfertasAdapter adapter;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    RecuperarOfertasAsync recuperarOfertasAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(this, "");
        setContentView(R.layout.activity_ofertas);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        txtEmpty = (TextView) findViewById(R.id.txtEmpty);

        adapter = new OfertasAdapter();
        adapter.setAdapterItemClickCallback(new OfertasAdapter.AdapterItemClickCallback() {
            @Override
            public void onAdapterClicked(FeedModel item) {
                if (item != null) {
                    Intent intent = new Intent(OfertasActivity.this, FullscreenImageActivity.class);
                    intent.putExtra("URL", item.getFotoUrl());
                    startActivity(intent);
                }
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ejecutarRequest();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
        recyclerView.setAdapter(new ScaleInAnimationAdapter(adapter));

        ejecutarRequest();
    }

    private void ejecutarRequest() {
        Connectivity connectionState = new Connectivity(this);
        if (!connectionState.isConnectedToInternet()) {
            Toast.makeText(this,
                    R.string.error_msg_no_estas_conectado_a_internet,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        recuperarOfertasAsync = new RecuperarOfertasAsync();
        recuperarOfertasAsync.mActivity = this;
        recuperarOfertasAsync.execute();
    }

    private void manejarRespuesta(ArrayList<FeedModel> result) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        if (result != null) {
            adapter.clear();
            txtEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            for (int i = 0; i < result.size(); i++) {
                FeedModel item = result.get(i);
                adapter.add(item);
            }
        } else {
            txtEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class RecuperarOfertasAsync
            extends AsyncTask<Void, Void, ArrayList<FeedModel>> {

        Gson gson = new Gson();
        OfertasActivity mActivity;
        TypeToken<ArrayList<FeedModel>> typeToken = new TypeToken<ArrayList<FeedModel>>() {
        };

        @SuppressWarnings({"TryWithIdenticalCatches", "LoopStatementThatDoesntLoop"})
        @Override
        protected ArrayList<FeedModel> doInBackground(Void... voids) {
            while (!isCancelled()) {
                try {

                    OkHttpClient okHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(Config.WEB_API.recuperarOfertas)
                            .build();

                    ArrayList<FeedModel> respuesta = null;
                    Response response = okHttpClient.newCall(request).execute();
                    if (response.isSuccessful()) {
                        Reader reader = response.body().charStream();
                        respuesta = gson.fromJson(reader, typeToken.getType());
                        reader.close();
                    }

                    return respuesta;
                } catch (IOException ignored) {
                    return null;
                } catch (JsonSyntaxException ignored) {
                    return null;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedModel> feedModels) {
            super.onPostExecute(feedModels);
            cancel(true);
            if (mActivity != null) {
                mActivity.manejarRespuesta(feedModels);
                mActivity.recuperarOfertasAsync = null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mActivity != null) {
                mActivity.swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.swipeRefreshLayout.setRefreshing(true);
                    }
                });
            }
        }
    }

}
