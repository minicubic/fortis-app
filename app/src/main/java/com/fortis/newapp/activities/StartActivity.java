package com.fortis.newapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.fortis.newapp.R;
import com.fortis.newapp.app.Config;
import com.fortis.newapp.helpers.LanguageHelper;
import io.fabric.sdk.android.Fabric;

public class StartActivity extends AppCompatActivity {

    private ImageView logo;
    private Button btnEspañol;
    private Button btnPortugues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_start);
        findViews();
    }

    private void findViews() {
        logo = (ImageView) findViewById(R.id.logo);
        btnEspañol = (Button) findViewById(R.id.btnEspañol);
        btnPortugues = (Button) findViewById(R.id.btnPortugues);

        btnEspañol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LanguageHelper.setLanguage(StartActivity.this, Config.IDIOMAS.Español);
                startActivity(new Intent(StartActivity.this, WelcomeActivity.class));
                StartActivity.this.finish();
            }
        });
        btnPortugues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LanguageHelper.setLanguage(StartActivity.this, Config.IDIOMAS.Portugues);
                startActivity(new Intent(StartActivity.this, WelcomeActivity.class));
                StartActivity.this.finish();
            }
        });
    }

}
