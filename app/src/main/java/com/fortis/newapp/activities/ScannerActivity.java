package com.fortis.newapp.activities;

import android.content.ContentResolver;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fortis.newapp.R;
import com.fortis.newapp.app.Config;
import com.fortis.newapp.customviews.WeakHandler;
import com.fortis.newapp.helpers.Connectivity;
import com.fortis.newapp.helpers.CurrencyHelper;
import com.fortis.newapp.helpers.LanguageHelper;
import com.fortis.newapp.models.ArticuloModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.Reader;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScannerActivity extends AppCompatActivity
        implements ZBarScannerView.ResultHandler {

    //https://github.com/dm77/barcodescanner

    private View layoutData;
    WeakHandler weakHandler;
    private ZBarScannerView mScannerView;
    private String TAG = "FortisScannerApp";
    private TextView txtArticuloDescripcion;
    private TextView txtMayorista;
    private TextView txtPrecioUnitarioGS;
    private TextView txtMayoristaGs;
    private TextView txtTotalMayoristaGs;
    private TextView txtPrecioUnitarioRs;
    private TextView txtMayoristaRs;
    private TextView txtTotalMayoristaRs;
    private TextView txtPrecioUnitarioUSD;
    private TextView txtMayoristaUSD;
    private TextView txtTotalMayoristaUSD;
    private ProgressBar progressBar;

    ConsultarArticuloAsync consultarArticuloAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(this, "");
        setContentView(R.layout.activity_scanner);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setupViews();
    }


    private void setupViews() {
        weakHandler = new WeakHandler();
        layoutData = findViewById(R.id.layoutData);
        txtArticuloDescripcion = (TextView) findViewById(R.id.txtArticuloDescripcion);
        txtMayorista = (TextView) findViewById(R.id.txtMayorista);
        txtPrecioUnitarioGS = (TextView) findViewById(R.id.txtPrecioUnitarioGS);
        txtMayoristaGs = (TextView) findViewById(R.id.txtMayoristaGs);
        txtTotalMayoristaGs = (TextView) findViewById(R.id.txtTotalMayoristaGs);
        txtPrecioUnitarioRs = (TextView) findViewById(R.id.txtPrecioUnitarioRs);
        txtMayoristaRs = (TextView) findViewById(R.id.txtMayoristaRs);
        txtTotalMayoristaRs = (TextView) findViewById(R.id.txtTotalMayoristaRs);
        txtPrecioUnitarioUSD = (TextView) findViewById(R.id.txtPrecioUnitarioUSD);
        txtMayoristaUSD = (TextView) findViewById(R.id.txtMayoristaUSD);
        txtTotalMayoristaUSD = (TextView) findViewById(R.id.txtTotalMayoristaUSD);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mScannerView = (ZBarScannerView) findViewById(R.id.scannerView);
        if (mScannerView != null) {

            mScannerView.setAutoFocus(true);
            weakHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        mScannerView.startCamera();
                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                }
            }, 500);
        }

        resetValues();
    }

    private void resetValues() {
        txtMayorista.setText("MAYORISTA");
        txtArticuloDescripcion.setText("");

        txtPrecioUnitarioGS.setText("0");
        txtPrecioUnitarioRs.setText("0");
        txtPrecioUnitarioUSD.setText("0");

        txtMayoristaGs.setText("0");
        txtMayoristaRs.setText("0");
        txtMayoristaUSD.setText("0");

        txtTotalMayoristaGs.setText("0");
        txtTotalMayoristaRs.setText("0");
        txtTotalMayoristaUSD.setText("0");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mScannerView != null) {
            // Register ourselves as a handler for scan results.
            mScannerView.setResultHandler(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.setResultHandler(null);
            // Stop camera on pause
            mScannerView.stopCamera();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        playScannerBeep();

//        Log.v(TAG, rawResult.getContents()); // Prints scan results
//        Log.v(TAG, rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        mScannerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ScannerActivity.this);
            }
        }, 3500);

        ejecutarRequest(rawResult.getContents());
    }

    private void playScannerBeep() {
        try {

            Uri beep = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                    "://" + getPackageName() + "/raw/store_scanner_beep");
            Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), beep);
            ringtone.play();

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private void ejecutarRequest(String codigoBarra) {
        Connectivity connectionState = new Connectivity(this);
        if (!connectionState.isConnectedToInternet()) {
            Toast.makeText(this,
                    R.string.error_msg_no_estas_conectado_a_internet,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        resetValues();
        progressBar.setVisibility(View.VISIBLE);

        consultarArticuloAsync = new ConsultarArticuloAsync(codigoBarra);
        consultarArticuloAsync.mActivity = this;
        consultarArticuloAsync.execute();
    }

    private void manejarRespuesta(ArticuloModel result) {
        progressBar.setVisibility(View.GONE);

        if (result != null) {
            txtArticuloDescripcion.setText(String.format("ART: %s", result.getDescripcion()));
            txtMayorista.setText(String.format("MAYORISTA (%s)", String.valueOf(result.getContenido())));

            txtPrecioUnitarioGS.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecio(), CurrencyHelper.Moneda.Guaranies));
            txtPrecioUnitarioRs.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecioUnitarioRS(), CurrencyHelper.Moneda.Reales));
            txtPrecioUnitarioUSD.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecioUnitarioUS(), CurrencyHelper.Moneda.Dolares));

            txtMayoristaGs.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecioVtaCaja(), CurrencyHelper.Moneda.Guaranies));
            txtMayoristaRs.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecioMayoristaRS(), CurrencyHelper.Moneda.Reales));
            txtMayoristaUSD.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getPrecioMayoristaUS(), CurrencyHelper.Moneda.Dolares));

            txtTotalMayoristaGs.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getTotalMayorista(), CurrencyHelper.Moneda.Guaranies));
            txtTotalMayoristaRs.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getTotalMayoristaRS(), CurrencyHelper.Moneda.Reales));
            txtTotalMayoristaUSD.setText(CurrencyHelper.getCurrencyFormattedString(
                    (double) result.getTotalMayoristaUS(), CurrencyHelper.Moneda.Dolares));

            weakHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    resetValues();
                }
            }, 7000);

        } else {
            resetValues();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class ConsultarArticuloAsync
            extends AsyncTask<Void, Void, ArticuloModel> {

        Gson gson = new Gson();
        ScannerActivity mActivity;
        TypeToken<ArticuloModel> typeToken = new TypeToken<ArticuloModel>() {
        };

        String mCodigoBarra = "";

        ConsultarArticuloAsync(String codBarra) {
            this.mCodigoBarra = codBarra;
        }

        @SuppressWarnings("TryWithIdenticalCatches")
        @Override
        protected ArticuloModel doInBackground(Void... voids) {
            try {

                String url = Config.WEB_API.consultarArticulo.replace("@codigobarra", mCodigoBarra);
                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                ArticuloModel respuesta = null;
                Response response = okHttpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    Reader reader = response.body().charStream();
                    respuesta = gson.fromJson(reader, typeToken.getType());
                    reader.close();
                }

                return respuesta;
            } catch (IOException ignored) {

            } catch (JsonSyntaxException ignored) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(ArticuloModel articuloModel) {
            super.onPostExecute(articuloModel);
            if (mActivity != null) {
                mActivity.manejarRespuesta(articuloModel);
                mActivity.consultarArticuloAsync = null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mActivity != null) {

            }
        }
    }
}
